# -*- coding: utf-8 -*-
"""
Created on Thu May  8 13:37:43 2014

@author: emmanuel
"""

from setuptools import setup, find_packages
from Cython.Build import cythonize
from Cython.Distutils import Extension


def make_extension(path):
    module = path[:-4].replace("/", ".")
    extension = Extension(module, [path], language="c++")
    return extension

setup(ext_modules=(cythonize([make_extension("connectome/pes/strong_path_enumeration_sampling.pyx"),
                              make_extension("connectome/pes/weak_path_enumeration_sampling.pyx"),
                              make_extension("util/urng.pyx"),
                              make_extension("connectome/shuffling/uniform_degree_preserving_shuffling.pyx"),
                              Extension("connectome.model.dynamicsorn.fast_cython_sorn",
                                        ["connectome/model/dynamicsorn/fast_cython_sorn.pyx"],
                                         extra_compile_args=["-fopenmp", "-O3"],
                                         extra_link_args=["-fopenmp"],
                                        language="c++")])),

      setup_requires=["cython"],
      install_requires=['brian2', 'cloudpickle', 'gitpython', 'luigi',
                        'matplotlib', 'networkx', 'numpy', 'openpyxl', 'redis',
                        'scikit-image', 'scikit-learn', 'seaborn', 'sqlalchemy'],

      include_package_data=True,
      packages=find_packages(exclude=["IPythonNotebooks*", "figures*", "misc*", "pydokuwiki*"]),
      entry_points={'console_scripts': ["discriminatEM = scripts.smcabc:main"]},

      description='Generation and analysis of cortical network models',
      author_email='emmanuel.klinger@brain.mpg.de',
      url="http://brain.mpg.de/connectomics",
      author='Emmanuel Klinger',
      name="discriminatEM",
      version="0.1.3",
      license="MIT",
      platforms="all")
