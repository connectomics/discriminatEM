from connectome.model.dynamicsorn import make_sorn
from connectome.model.dynamicsorn.sorn_inputs import RepeatingPatterns, sorn_input
from connectome.analysis import standard_analysis
from connectome.model import Network
import pandas as pd
from parallel import SGE
import os
sge = SGE(num_threads=4, priority=0, name="sorn", chunk_size=4)


def pattern_from_str(s):
    if s == "orig":
        return sorn_input
    _, n, _, f = s.split("_")
    return RepeatingPatterns(nr_patterns=int(n), sparsity=float(f))


pattern_strings = (["orig"] + [f"rep_{n}_sparse_{f}" for n in [1, 10, 100, 250, 500, 1000, 10000] for f in [.1, .5, 1]]) * 10


patterns = list(map(pattern_from_str, pattern_strings))
adjacencies = sge.map(lambda x: make_sorn(input_generator=x), patterns)
analyses = [standard_analysis(Network(adj)) for adj in adjacencies]

df = pd.DataFrame(analyses)
df["pattern"] = pattern_strings

df.to_json(os.path.expanduser("~/data/sorn_input_patterns.json"))
