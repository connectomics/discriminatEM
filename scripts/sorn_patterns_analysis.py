#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 19:32:55 2017

@author: emmanuel
"""

import pandas as pd
import seaborn as sns
import scipy as sp
sns.set_style("white")

df = pd.read_json("/home/emmanuel/WritingProjects/NetworkanalysisPaper/figures/data/sorn_input_patterns.json")
df = df.sort_values("pattern")
melted = pd.melt(df, "pattern")

#%%
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
fig.set_size_inches((25,6))
sns.barplot(x="variable", y="value", hue="pattern", data=melted, ax=ax)
plt.setp(ax.xaxis.get_majorticklabels(), rotation=45);

        
#%%
interesting = df[["in_out_degree_correlation_exc", "reciprocity_ee", "relative_reciprocity_ee", "pattern"]]
pars = interesting.pattern.apply(lambda x: pd.Series(x.split("_")[1::2] if x != "orig" else [sp.inf, 1],
                                                     index=["patterns", "sparsity"]))

interesting = pd.concat((interesting, pars), axis=1)
interesting["patterns"] = pd.to_numeric(interesting.patterns)
interesting["sparsity"] = pd.to_numeric(interesting.sparsity)
del interesting["pattern"]

interesting_melted = pd.melt(interesting, ["patterns", "sparsity"])


sns.factorplot(x="sparsity", y="value", hue="patterns", data=interesting_melted,
               col="variable", kind="bar")