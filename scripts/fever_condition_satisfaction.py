from connectome.model import ERFEVER
import numpy.linalg as la

n_neurons = 100
p_exc = .2


def print_stuff(f_dim_fract, feverization_ratio):
    f = ERFEVER(p_exc=p_exc, p_inh=.5, nr_neurons=n_neurons,
                feverization_ratio=feverization_ratio,
                inh_ratio=0,
                feature_space_dimension=int(n_neurons*f_dim_fract*p_exc))


    net = f()

    adj = net.adjacency_matrix

    fv = f.get_feature_vectors()

    norm = la.norm(fv - fv @ adj, 2)
    print(f)
    print("norm", norm)
    print("init conn", f.fever_initial_connectivity_)
    return {"norm": norm, "fdim": f.feature_space_dimension,
            "p_init":f.fever_initial_connectivity_.p_initial_graph_exc }


res = []
for k in range(2):
    r = print_stuff(.5, .7)
    res.append(r)
    r = print_stuff(1, 1)
    res.append(r)

import pandas as pd
df = pd.DataFrame(res)

print(df)
print(df.groupby("p_init")["norm"].mean())