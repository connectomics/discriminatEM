library(ggplot2)
library(stringr)

df <- read.csv("/home/emmanuel/tmp/sorn.csv")

dfplot = df[,c("reciprocity_ee", "in_out_degree_correlation_exc", "pattern")]

splitted <- str_split_fixed(dfplot$pattern, "_", 4)[,c(2,4)]
colnames(splitted) <- c("rep", "spar")

together <- cbind(dfplot, splitted)
together$pattern <- NULL

(ggplot(together, aes(rep,  reciprocity_ee, colour=spar)) + geom_boxplot() + theme_bw()
 + ggsave(file="/home/emmanuel/tmp/test.pdf", width=4, height=2))