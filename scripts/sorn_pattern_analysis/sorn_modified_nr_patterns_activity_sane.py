from connectome.function import UnsynchronizedActivityTask
from connectome.model import SORN, default_mean_par
import joblib
import os
cache = joblib.Memory(os.path.expanduser("~/joblib_cache")).cache

nr_patterns = [1, 250, 500, 1000, 2000, -1]


@cache
def run_sorn(nr_patterns):
    sorn = SORN(nr_neurons=2000, inh_ratio=.1, p_exc=.2, p_inh=.6,
                eta_stdp=default_mean_par("SORN", "eta_stdp"),
                eta_intrinsic=default_mean_par("SORN", "eta_intrinsic"),
                nr_patterns=nr_patterns)

    task = UnsynchronizedActivityTask()
    res = task(sorn)
    return res

results = list(map(run_sorn, nr_patterns))
