import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style("white")

df = pd.read_csv("/home/emmanuel/tmp/sorn.csv")

dfplot = df[["reciprocity_ee", "in_out_degree_correlation_exc", "pattern"]]

splitted = df.pattern.str.split("_", expand=True)[[1, 3]]
splitted.columns = ["rep", "spar"]

together = pd.concat((dfplot, splitted), axis=1)
del together["pattern"]

sns.boxplot("rep", "reciprocity_ee", "spar", data=together.fillna(float("inf")))
plt.savefig("/home/emmanuel/tmp/sornpy.pdf")