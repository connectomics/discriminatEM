import argparse
from connectome.abctask import ABCConnectomeTask, execute_task_list, GroundTruthData
from parallel import SGE, sge_available
from connectome.model import SORN, default_definitions, default_mean_par


argument_parser = argparse.ArgumentParser(description="SMC ABC")
argument_parser.add_argument("db", type=str)
argument_parser.add_argument("--nr_parallel_jobs", type=int, default=6, help="Default: 7")
args = argument_parser.parse_args()

mapper = SGE(memory='4G', name="sorn-abc").map if sge_available() else map


sorn = SORN(nr_neurons=2000, inh_ratio=.1, p_exc=.2, p_inh=.6,
            eta_stdp=default_mean_par("SORN", "eta_stdp"),
            eta_intrinsic=default_mean_par("SORN", "eta_intrinsic"),
            nr_patterns=-1)

ground_truth_samples = [GroundTruthData(sorn(nr_patterns=nr_patterns), -1, {"nr_patterns": float(nr_patterns)})
                        for nr_patterns in [1, 250, 500, 1000, 2000, -1]]


print("Start ABC Tasks", flush=True)
abc_tasks = [ABCConnectomeTask(default_definitions(), gt_sample, args.db, mapper) for gt_sample in ground_truth_samples]
execute_task_list(abc_tasks, args.nr_parallel_jobs)
