# Code Repository for
**Cellular connectomes as arbiters of local circuit models in the cerebral cortex**  
Emmanuel Klinger, Alessandro Motta, Carsten Marr, Fabian J. Theis, Moritz Helmstaedter.  
Nature Communications (2021) DOI: [10.1038/s41467-021-22856-z](https://www.nature.com/articles/s41467-021-22856-z)

## Documentation and more
The [DiscriminatEM website](https://discriminatem.brain.mpg.de/)
* hosts documentation and a user's guide for this code repository,
* links to the research article and supplementary informations,
* and serves all data that support the findings in this study.

## Getting started
The code repository can be set up as follows.

These instructions assume a working installation of
[Anaconda](https://docs.anaconda.com/anaconda/install/) or
[Miniconda](https://docs.conda.io/en/latest/miniconda.html).

```
git clone https://gitlab.mpcdf.mpg.de/connectomics/discriminatEM.git
cd discriminatEM

conda create -n discriminatem python=3.7 cython
conda activate discriminatem
pip install -e .
```

For next steps, please see the
[documentation](https://discriminatem.brain.mpg.de/doc/latest/).

## Note
* GCC must be new enough. GCC 6.4 works. With GCC 4.7, compilation of
  `connectome/pes/strong_path_enumeration_sampling.cpp` failed with:

  ```
  error: ‘mt19937’ in namespace ‘std’ does not name a type
  error: ‘uniform_real_distribution’ in namespace ‘std’ does not name a type
  ```

* For users of Anaconda: Anaconda comes with its own linker executable.
  This linker is relatively old. If your version of the system C / C++
  compiler is much newer than that, it's possible that "cythonization"
  will fail because Anaconda's linker is unable to parse the code
  objects produced by compilers. As a workaround, just rename or remove
  the `compiler_compat/ld` file in the activated Conda environment.


## Provenience
This is a copy of  
git@gitlab.mpcdf.mpg.de:connectomics/Emmanuel.git  
commit 647cee12eac354a75cdc97e175d279236a5ecb70
