"""
Modelflow framework
===================

Some functional elements in Python.


This framework had two motivations:

1. Simple partial function evaluation patterns
2. Graph structured pipelines.
"""

from .node import Node
from modelflow.input import Input
from .elementarynodes import Plus, Minus, Division, Identity, InputChannel
from .randomvariable import RandomVariable
from .dictionarymerger import DictionaryMerger
from .graph import Graph, DynamicGraph, StaticGraph


__all__ = ["Node",
           "Input",
           "Plus", "Minus", "Division", "Identity", "DictionaryMerger",
           "Graph", "DynamicGraph", "StaticGraph"]
