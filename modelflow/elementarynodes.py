from .input import Input
from .node import Node


class Minus(Node):
    plus = Input()
    minus = Input()

    def run(self):
        return self.plus - self.minus


class Plus(Node):
    """
    **Returns:** arg1 + arg2
    """
    arg1 = Input()
    arg2 = Input()

    def run(self):
        return self.arg1 + self.arg2


class Division(Node):
    """
    **Returns:** enumerator / denominator
    """
    enumerator = Input()  #: enumerator
    denominator = Input()  #: denominator

    def run(self):
        return self.enumerator / self.denominator


class Identity(Node):
    """
    **Returns:** value
    """
    value = Input()  #: value

    def run(self):
        return self.value


class MergeTwoDictionaries(Node):
    dictionary_1 = Input()
    dictionary_2 = Input()

    def run(self):
        result = self.dictionary_1.copy()
        result.update(self.dictionary_2)
        return result


class InputChannel(Identity):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def __repr__(self):
        return "<" + self.__class__.__name__ + ": " + str(self.name) + "=" + str(self.value) + ">"