import scipy.stats

from modelflow import Node, Input


class RandomVariable(Node):
    type = Input()
    args = Input()
    kwargs = Input()

    def run(self):
        return getattr(scipy.stats, self.type)(*self.args, **self.kwargs).rvs()