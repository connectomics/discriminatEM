from .graph import DynamicGraph
from .elementarynodes import MergeTwoDictionaries


class DictionaryMerger(DynamicGraph):
    def __init__(self, nr_dictionaries):
        super().__init__()
        self.add_inputs([self.input_name(k) for k in range(nr_dictionaries)])
        current_dict = MergeTwoDictionaries()
        self.connect_input_to_node(self.input_name(0), current_dict, "dictionary_1")
        self.connect_input_to_node(self.input_name(1), current_dict, "dictionary_2")
        for k in range(2, nr_dictionaries):
            new_dict = MergeTwoDictionaries()
            self.connect_node_to_node(current_dict, new_dict, "dictionary_1")
            self.connect_input_to_node(self.input_name(k), new_dict, "dictionary_2")
            current_dict = new_dict
        self.set_output(current_dict)

    def input_name(self, nr):
        return "in_" + str(nr)