import unittest

from modelflow import DictionaryMerger
from modelflow.elementarynodes import MergeTwoDictionaries


class TestMergeTwoDictionaries(unittest.TestCase):
    def test_merge_two_dicts(self):
        dictionary_merger = MergeTwoDictionaries()
        dict1 = {"key_1_a": "value_1_a",
                 "key_1_b": "value_1_b"}
        dict2 = {"key_2_a": "value_2_a",
                 "key_2_b": "value_2_b"}
        expected = {"key_1_a": "value_1_a",
                    "key_1_b": "value_1_b",
                    "key_2_a": "value_2_a",
                    "key_2_b": "value_2_b"}
        result = dictionary_merger(dictionary_1=dict1, dictionary_2=dict2)
        self.assertEqual(result, expected)


class TestDictionaryMerger(unittest.TestCase):
    def test_merger(self):
        dictionary_merger = DictionaryMerger(4)
        dict1 = {"key_1_a": "value_1_a",
                 "key_1_b": "value_1_b"}
        dict2 = {"key_2_a": "value_2_a",
                 "key_2_b": "value_2_b"}
        dict3 = {"key_3_a": "value_3_a",
                 "key_3_b": "value_3_b",}
        dict4 = {"key_4_a": "value_4_a",
                 "key_4_b": "value_4_b"}
        expected = {"key_1_a": "value_1_a",
                    "key_1_b": "value_1_b",
                    "key_2_a": "value_2_a",
                    "key_2_b": "value_2_b",
                    "key_3_a": "value_3_a",
                    "key_3_b": "value_3_b",
                    "key_4_a": "value_4_a",
                    "key_4_b": "value_4_b"}
        result = dictionary_merger(in_0=dict1, in_1=dict2, in_2=dict3, in_3=dict4)
        self.assertEqual(result, expected)