import unittest

from modelflow import Input, Node



class TestModelMixin(unittest.TestCase):
    def test_mixin(self):
        class Mixin:
            x = Input()

        class Derived(Mixin, Node):
            def run(self):
                return self.x*2

        derived = Derived(x=3)
        self.assertEqual(6, derived())


if __name__ == "__main__":
    unittest.main()
