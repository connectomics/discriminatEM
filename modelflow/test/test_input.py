import unittest
from modelflow import Input, Node


class TestElementaryUnits(unittest.TestCase):
    def test_default_input(self):
        default_value = "default_text"

        class WithDefault(Node):
            a = Input("default_text")

            def run(self):
                return self.a

        with_default = WithDefault()
        self.assertEqual(default_value, with_default())


if __name__ == "__main__":
    unittest.main()
