import unittest
from modelflow import RandomVariable


class TestRandomVariable(unittest.TestCase):
    def test_random_variable(self):
        rv = RandomVariable(type="uniform", args=[1, .1], kwargs={})
        self.assertTrue(1 <= rv() <= 1.1)