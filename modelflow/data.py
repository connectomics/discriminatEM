class Data:
    pass


class NoData(Data):
    """
    Implements a NullObject pattern for the inputs.
    Unpacking is required if input type is expected to be iterable
    of a mapping
    """
    def __new__(cls, *args, **kwargs):
        if hasattr(cls, "obj"):
            return cls.obj
        obj = super().__new__(cls)
        cls.obj = obj
        return cls.obj

    def __repr__(self):
        return "∅"

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration

    def __getitem__(self, key):
        return None

    def keys(self):
        return self
