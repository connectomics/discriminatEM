class Input:
    """
    Attribute descriptor for inputs.

    Think of this as function arguments.
    """
    def __init__(self, default=None):
        self.name = None
        self.default = default

    def __get__(self, instance, owner):
        try:
            return instance._inputs[self.name]
        except KeyError:
            return self.default

    def __set__(self, instance, value):
        raise Exception("Input properties are read only: " + str(self.name))
