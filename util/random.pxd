cdef extern from "<random>" namespace "std" nogil:
    cdef cppclass mt19937:
        mt19937()
        mt19937(long)
        unsigned int min()
        unsigned int max()
        unsigned int get "operator()"()

    cdef cppclass uniform_real_distribution[T]:
        uniform_real_distribution(double, double)
        T get "operator()"(mt19937&)
