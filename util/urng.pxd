from util.random cimport mt19937, uniform_real_distribution

cdef class URNG:
    cdef:
        mt19937 *engine
        uniform_real_distribution[double] *uniform

    cpdef double rvs(self)