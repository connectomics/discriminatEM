"""
Usage


python population_progress.py <databasefile> <outputplog>
"""


import pandas as pd
import scipy as sp
import sys

db = sys.argv[1]
outimg = sys.argv[2]

pops = pd.read_sql_table("populations", "sqlite:///" + db)
abcsmc = pd.read_sql_table("abc_smc", "sqlite:///" + db)

pops = pops.sort_values("population_end_time")
pops["Estimated Progress %"] = sp.arange(0, len(pops)) / (len(abcsmc) * 8) * 100

ax =  pops.plot("population_end_time", "Estimated Progress %")

ax.set_ylim(0, 100)
ax.set_xlabel("Time")
ax.get_figure().savefig(outimg, bbox_inches="tight")
