import os
import unittest


def run_slow_test():
    try:
        if os.environ["RUN_LONG_TESTS"].lower() == "true":
            return True
    except KeyError:
        return False


def run_test(file):
    module = os.path.dirname(file)
    suite = unittest.defaultTestLoader.discover(module)
    runner = unittest.TextTestRunner()
    runner.run(suite)
