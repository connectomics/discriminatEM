from cython.operator cimport dereference
from .random cimport mt19937, uniform_real_distribution


cdef class URNG:
    def __cinit__(self, long seed, double lower, double upper):
        self.engine = new mt19937(seed)
        self.uniform = new uniform_real_distribution[double](lower, upper)

    def __dealloc__(self):
        del self.engine
        del self.uniform

    cpdef double rvs(self):
        return self.uniform.get(dereference(self.engine))
