from .blockmodel import BiologicalBlockmodel
from modelflow import Node, Input
from .bases import NetworkModelMixin


class FF(NetworkModelMixin, Node):
    nr_exc_subpopulations = Input()
    reciprocity_exc = Input()

    def run(self):
        network = BiologicalBlockmodel(p_inh=self.p_inh,
                             p_exc=self.p_exc,
                             nr_neurons=self.nr_exc,
                             inh_ratio=self.inh_ratio,
                             nr_upper_diagonals=self.nr_exc_subpopulations,
                             nr_exc_subpopulations=self.nr_exc_subpopulations,
                             reciprocity_exc=self.reciprocity_exc)()
        return network
