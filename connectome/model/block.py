from collections import UserList


class Block:
    def __init__(self, size: int, kind: str):
        """
        Parameters
        ----------
        size: int
            Nr of neurons in block
        kind: str
            "E" or "I"
        """
        self.size = size
        self.kind = kind

    def __eq__(self, other):
        return self.size == other.size and self.kind.lower() == other.kind.lower()

    def __repr__(self):
        return "{}({}, {})".format(self.__class__.__name__, self.size, self.kind)


class BlockStructure(UserList):
    def start_end(self, index):
        """
        Parameters
        ----------
        index: int
            The number of the block.

        Returns
        -------
        start, end: int, int
            Start and end neuron index of the block.
        """
        start = sum(block.size for block in self[:index])
        end = sum(block.size for block in self[:index+1])
        return start, end

    @property
    def nr_exc(self):
        """
        Number of excitatory blocks.
        """
        return len([block for block in self if block.kind.lower() == "e"])

    @property
    def nr_inh(self):
        """
        Number of inhibitory blocks.
        """
        return len([block for block in self if block.kind.lower() == "i"])
