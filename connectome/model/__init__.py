from .api import API
from .er import ER
from .exp import EXP
from .fever import ERFEVERInitialConnectivity, EXPFEVERInitialConnectivity, ERFEVER, EXPFEVER
from .ff import FF
from .ll import LL
from .simplemiller import SimpleMiller
from .sorn import SORN
from .syn import SYN
from .network import Network, save_normalize


__all__ = ["ER", "EXP", "LL", "SYN", "SORN", "API", "ERFEVER", "default_definitions"]


def default_definitions():
    """
    Returns
    -------
    default_definitions: dict
        Default model definitions
    """


    expectedsha512sum = ('f224e9f2aafe0ad8c3628ee16e9f427214f87c9453ddee3d'
                         '427f72741384a8d0da081067ccd3e15f6eca133746a8b921d'
                         '1c57448715212b250865aa2a7315bff')
    import json
    import os
    import hashlib
    file = os.path.join(os.path.dirname(__file__), "definitions.json")
    with open(file) as f:
        content = f.read()

    assert hashlib.sha512(content.encode()).hexdigest() == expectedsha512sum, "Model definitions corrupted"

    with open(file) as f:
        default_definitions = json.load(f)

    return default_definitions


def default_mean_par(model_name, par_name):
    sorn_par = (default_definitions()["models"]
                ["model_specific_parameters"]
                [model_name]
                [par_name]
                ["args"])
    return sorn_par[0] + sorn_par[1] / 2
