import scipy as sp
from numpy import linalg as la
from modelflow import Node, Input


def uniform_spherical_feature_fectors(nr_neurons, feature_space_dimension):
    """
    Uses that vectors with Gaussian distributions of their entries
    are uniformly distributed on the unit sphere after normalization
    to length 1.
    """
    feature_vectors = sp.randn(feature_space_dimension, nr_neurons)
    feature_vectors /= la.norm(feature_vectors, axis=0, keepdims=True)
    return feature_vectors

