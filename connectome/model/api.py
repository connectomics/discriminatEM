from .correlationbasednetwork import NegativeCorrelationRelaxingNetwork
from .util import diag_to_zero


class API(NegativeCorrelationRelaxingNetwork):
    """
    The antiphase inhibition network.

    It has the additional parameters

    :param n_pow: float
        Determines how correlations are converted to connection probabilities.
    :param feature_space_dimension: int
        Dimension of the underlying feature space.
    """

    def signed_to_positive_correlations(self, signed_correlations):
        """
        Modify according to
        (correlation + 1) / 2.
        """

        relaxed_correlations = signed_correlations.copy()
        relaxed_correlations = (relaxed_correlations + 1) / 2

        # to deal with possible floating point precision problems
        relaxed_correlations[relaxed_correlations >= 1.] = 1.
        relaxed_correlations[relaxed_correlations <= 0.] = 0.
        diag_to_zero(relaxed_correlations)
        return relaxed_correlations
