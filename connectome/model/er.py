from modelflow import Node
from .bases import NetworkModelMixin, connect_uniform, Network


class ER(NetworkModelMixin, Node):
    """
    Directed Erdős–Rényi network with excitatory and inhibitory subpopulations.
    """
    def run(self):
        net = Network(nr_exc=self.nr_exc, nr_inh=self.nr_inh)
        for post in "EI":
            connect_uniform(net, "E", post, self.p_exc)
        for post in "EI":
            connect_uniform(net, "I", post, self.p_inh)
        return net