from .util import diag_to_zero
from .correlationbasednetwork import NegativeCorrelationRelaxingNetwork


class SimpleMiller(NegativeCorrelationRelaxingNetwork):
    def signed_to_positive_correlations(self, signed_correlations):
        """
        Negative signed correlations are set to zero probability.
        """
        relaxed_correlations = signed_correlations.copy()
        relaxed_correlations[relaxed_correlations <= 0] = 0
        diag_to_zero(relaxed_correlations)
        return relaxed_correlations
