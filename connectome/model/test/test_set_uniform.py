import unittest

from connectome.model.bases import connect_uniform

from connectome.model.network import Network


class TestSetUniForm(unittest.TestCase):
    def test_uniform_inh(self):
        net = Network(nr_exc=1000, nr_inh=1000)
        connect_uniform(net, "I", "E", .5)
        expected_nr_edges = 1000**2*.5
        self.assertLess(abs(-net.adjacency_matrix.sum() - expected_nr_edges) / expected_nr_edges, .01)

    def test_uniform_exc(self):
        net = Network(nr_exc=1000, nr_inh=1000)
        connect_uniform(net, "E", "E", .5)
        expected_nr_edges = 1000**2*.5
        self.assertLess(abs(net.adjacency_matrix.sum() - expected_nr_edges) / expected_nr_edges, .01)


if __name__ == '__main__':
    unittest.main()