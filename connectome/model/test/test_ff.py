import unittest

import scipy as sp

from connectome.analysis import ConnectivityEstimator, ReciprocityEstimator
from connectome.model import FF

class TestFF(unittest.TestCase):
    def make(self, nr_exc_subpopulations=4):
        self.p_exc =.2
        self.reciprocity_exc = .15
        self.nr_exc_subpopulations = nr_exc_subpopulations
        self.ff = FF(nr_neurons=2000, inh_ratio=.2, p_exc=self.p_exc, p_inh=.5,
                     nr_exc_subpopulations=self.nr_exc_subpopulations, reciprocity_exc=self.reciprocity_exc)
        self.net = self.ff()
        self.conn = ConnectivityEstimator(network=self.net)()
        self.rec = ReciprocityEstimator(network=self.net)()

    def test_connectivity(self):
        self.make()
        self.assertLess(abs(self.conn["p_ee"]/self.p_exc - 1), .01)

    def test_reciprocity(self):
        self.make()
        self.assertLess(abs(self.rec["reciprocity_ee"] / self.reciprocity_exc - 1), .02)

    def test_high_reciprocity(self):
        self.make(7)
        self.assertLess(abs(1-self.conn["p_ee"]/self.p_exc), 0.02)

    def test_diag_zero(self):
        self.make(3)
        self.assertFalse(sp.diag(self.net.adjacency_matrix).any())