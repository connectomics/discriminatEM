import unittest
from connectome.model import SORN


class TestSORNNetwork(unittest.TestCase):
    def test_diag_zero(self):
        sorn = SORN(nr_neurons=200, inh_ratio=.1, p_exc=.2, p_inh=.2, eta_stdp=0.0001, eta_intrinsic=0.1)
        net = sorn()
        self.assertFalse(net.adjacency_matrix.diagonal().any())

    def test_right_time_steps(self):
        self.assertEqual(SORN.NR_TIME_STEPS, 10_000)


if __name__ == '__main__':
    unittest.main()
