import unittest

import scipy as sp

from connectome.model.featurevector import uniform_spherical_feature_fectors


class TestUniformSphericalFeatureVectors(unittest.TestCase):
    def test_normalized(self):
        vectors = uniform_spherical_feature_fectors(500, 50)
        self.assertTrue(sp.isclose((vectors**2).sum(axis=0), 1).all())


if __name__ == "__main__":
    unittest.main()