import unittest
from connectome.model import SimpleMiller, API
import scipy as sp


class TestCorrelationBasedWeightDistributionPathology(unittest.TestCase):
    """
    Test if inh synapse weight are not all the same.
    This might happen in some pathological cases
    """
    def test_inh_weights_not_all_equal_simple_miller(self):
        n_pow = 4
        p_exc = .15
        p_inh = .3
        feature_space_dimension = 200  # nr_exc // 10
        sm = SimpleMiller(nr_neurons=2000, inh_ratio=.1, p_exc=p_exc, feature_space_dimension=feature_space_dimension,
                          p_inh=p_inh, n_pow=n_pow)
        net = sm()
        nr_inh_syn_weights = len(sp.unique(net["I", :]))
        self.assertGreater(nr_inh_syn_weights, 10)

    def test_inh_weights_not_all_equal_API(self):
        n_pow = 4
        p_exc = .15
        p_inh = .5
        feature_space_dimension = 200  # nr_exc // 10
        sm = API(nr_neurons=2000, inh_ratio=.1, p_exc=p_exc, feature_space_dimension=feature_space_dimension,
                 p_inh=p_inh, n_pow=n_pow)
        net = sm()
        nr_inh_syn_weights = len(sp.unique(net["I", :]))
        self.assertGreater(nr_inh_syn_weights, 10)

if __name__ == "__main__":
    unittest.main()