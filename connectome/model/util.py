import scipy as sp


def diag_to_zero(arr):
    """

    Parameters
    ----------
    arr: numpy array

    Sets the diagonal of an array to 0.
    """
    diag = sp.diag(arr)
    diag.flags["WRITEABLE"] = True
    diag[:] = 0
