import unittest
from connectome.model.dynamicsorn import make_sorn
import numpy as np


class TestNegativeExcitatorySynapses(unittest.TestCase):
    def test_no_negative_exc_synapses(self):
        ne = 50
        adj = make_sorn(nr_exc=ne, nr_inh=20, p_exc=.1, p_inh=.46)
        self.assertFalse((adj[:,:ne] < 0).any())

    def test_not_nan(self):
        ne = 50
        adj = make_sorn(nr_exc=ne, nr_inh=20, p_exc=.1, p_inh=.46)
        self.assertTrue(np.isfinite(adj).all())

if __name__ == "__main__":
    unittest.main()