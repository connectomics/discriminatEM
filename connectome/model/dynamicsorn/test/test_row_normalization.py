import unittest

import scipy as sp

from connectome.model.dynamicsorn.fast_cython_sorn import make_sorn


class TestNormalization(unittest.TestCase):
    def test_normalization(self):
        nr_exc = 200
        nr_inh = 50
        adj, row_sum, delta_row_sum = make_sorn(300, nr_exc, nr_inh, .1, .2 , .5, .5, -.5, debug=True)
        self.assertLess(sp.absolute(adj[:nr_exc,:nr_exc].sum(axis=1) - row_sum - delta_row_sum).sum() / nr_exc, 0.01)