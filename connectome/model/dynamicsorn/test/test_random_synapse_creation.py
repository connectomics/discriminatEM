import unittest

import scipy as sp

from connectome.model.dynamicsorn.fast_cython_sorn import add_random_synapses, make_sorn


class TastRandomSynapseAddition(unittest.TestCase):
    def test_synapses_added(self):
        n = 2000
        adjacency = sp.zeros((n,n))
        nr_add_attempts = 2000*2 * .1
        delta_row_sum = sp.zeros(n)
        add_random_synapses(adjacency, delta_row_sum, nr_add_attempts, 1.)
        self.assertGreaterEqual(adjacency.sum(), 10)

    def test_connectivity(self):
        nr_exc = 2000
        nr_inh = 200
        nr_time_steps = 100
        adj,  row_sum, delta_row_sum = make_sorn(nr_time_steps, nr_exc, nr_inh, .1, .2 , .5, .5, -.5, debug=True)
        self.assertLess(abs((adj[:nr_exc,:nr_exc] != 0).sum() / nr_exc**2 -.1) / .1, 0.05)

    def test_sign(self):
        nr_exc = 200
        nr_inh = 100
        nr_time_steps = 100
        adj,  row_sum, delta_row_sum = make_sorn(nr_time_steps, nr_exc, nr_inh, .1, .2 , .5, .5, -.5, debug=True)
        self.assertTrue((adj[:,:nr_exc] >=0).all())
        self.assertTrue((adj[:,nr_exc:] <=0).all())