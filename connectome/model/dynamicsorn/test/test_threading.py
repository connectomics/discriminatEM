import unittest

from connectome.model.dynamicsorn.fast_cython_sorn import test_threading


class TestThreading(unittest.TestCase):
    def test_threading(self):
        res = test_threading()
        self.assertGreaterEqual(len(res), 1)