import unittest
from connectome.model.dynamicsorn.sorn_inputs import RepeatingPatterns


class TestSORNPLausibleValues(unittest.TestCase):
    def test_enough_pattern(self):
        patterns = RepeatingPatterns(nr_patterns=3)
        for _ in range(20):
            res = patterns(10)
            self.assertEqual(10, res.size)

    def test_repetition(self):
        patterns = RepeatingPatterns(nr_patterns=3)
        pattern_list = [patterns(10) for _ in range(20)]
        self.assertTrue((pattern_list[0] == pattern_list[3]).all())


if __name__ == "__main__":
    unittest.main()
