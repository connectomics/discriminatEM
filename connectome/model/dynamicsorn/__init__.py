from .fast_cython_sorn import make_sorn, NullRecorder
from .sorn_inputs import RepeatingPatterns