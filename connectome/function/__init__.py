"""
Functional model testing.
"""

from connectome.function.task.memory import MemoryTask
from .criterion import Range, GreaterThan, LessThan, TrueCriterion
from .modeltest import TestSuite, TestResult
from .task import TuningTask, UnsynchronizedActivityTask, MemoryTask, PropagationTask, TextureTask

