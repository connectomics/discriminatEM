"""
Keras extensions
================

.. automodule:: .trainblocks
   :members:
"""

from .constraints import Clip
from .layers import MySimpleRnn, BlockRnn, slice_last
from .blockstructures import BlockMatrix, BlockVector, BT
from .optimizers import Adagrad, Adam, RMSprop, SGD