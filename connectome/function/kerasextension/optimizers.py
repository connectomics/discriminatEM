"""
Patched optimizers. They also include clipnorm and clipvalue in the output.
"""
import keras.optimizers as keras_original_optimizers


class ClipMixin:
    """
    Mixing class for output of clinorm and clipvalue in get_config
    """
    def get_config(self):
        config = {}
        try:
            clipnorm = self.clipnorm
        except AttributeError:
            pass
        else:
            config["clipnorm"] = clipnorm

        try:
            clipvalue = self.clipvalue
        except AttributeError:
            pass
        else:
            config["clipvalue"] = clipvalue

        config.update(super().get_config())
        return config


class Adam(ClipMixin, keras_original_optimizers.Adam):
    pass


class RMSprop(ClipMixin, keras_original_optimizers.RMSprop):
    pass


class SGD(ClipMixin, keras_original_optimizers.SGD):
    pass


class Adagrad(ClipMixin, keras_original_optimizers.Adagrad):
    pass
