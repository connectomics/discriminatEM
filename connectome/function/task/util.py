from functools import wraps


def add_default_output(f):
    @wraps(f)
    def wrapper(self, model):
        output = f(self, model)
        for key, value in model.inputs.items():
            if key not in output:
                output[key] = value
        if "model_name" not in output:
            output["model_name"] = model.__class__.__name__
        return output
    return wrapper
