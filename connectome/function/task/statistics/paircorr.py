"""
Spiking neuron autocorrelation
"""

import scipy as sp
import numpy as np


def pair_correlation(history: np.ndarray, *, accumulator=sp.median) -> float:
    """

    Parameters
    ----------
    history: 2D array
    accumulator: function
        convert the pairwise correlation distribution into a single value with this summary statistic
        Can be somehting lik

        * scipy.median
        * scipy.mean

    Returns
    -------

    correlation: float
        The correlation
    """
    corrcoef = sp.corrcoef(history, rowvar=False)
    indices = sp.tril_indices(corrcoef.shape[0], 1)
    values = corrcoef[indices]
    accumulated = accumulator(values)
    return float(accumulated)

