import unittest

import scipy as sp

from connectome.function.task.statistics import population_cv


class TestPopulationCV(unittest.TestCase):
    def test_cv_columns(self):
        """
        Check I got the axis right.
        """
        arr = sp.zeros((100, 10))
        arr[:, 0] = 1
        arr[:, 2] = 1
        arr[:,-1] = 1
        cv = population_cv(arr)
        cv_T = population_cv(arr.T)
        self.assertEqual(0, cv)
        self.assertNotEqual(0, cv_T)

    def test_cv_rows(self):
        """
        Check I got the axis right.
        """
        arr = sp.zeros((100, 10))
        val = sp.rand(100) > .5
        for k in range(10):
            arr[:,k] = val
        cv = population_cv(arr)
        cv_T = population_cv(arr.T)
        self.assertEqual(0, cv_T)
        self.assertNotEqual(0, cv)


if __name__ == "__main__":
    unittest.main()
