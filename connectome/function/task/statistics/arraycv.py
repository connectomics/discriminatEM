"""
CV of an array
"""

import scipy as sp


def cv(inter_spike_times):
    mean = inter_spike_times.mean()
    std = inter_spike_times.std()
    return std / mean


def population_cv(arr):
    inter_spike_times = []
    for trace in arr.T:
        spike_times = sp.nonzero(trace)[0]
        inter_spike_times.append(sp.diff(spike_times))
    inter_spike_times_concatenated = sp.concatenate(inter_spike_times)
    return cv(inter_spike_times_concatenated)