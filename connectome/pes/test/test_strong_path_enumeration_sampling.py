# -*- coding: utf-8 -*-
"""
Created on Thu May  8 14:40:12 2014

@author: emmanuel
"""

import unittest

import scipy as sp

from connectome.pes import strong_path_enumeration_sampling as strong_pes


def convert_array(arr):
    return list(arr)


class TestStrongPES(unittest.TestCase):
    def sample_strong(self, matrix):
        n = matrix.shape[0]
        paths, cycles = strong_pes.pes(matrix, n, probabilities=[1] * n, repeat=1, ratio_paths_to_nodes=1000)
        return paths, cycles

    def test_strong_pes_triangle(self):
        triangle = sp.array( [ [0 , 0 , 1],
                                [1 , 0 , 0],
                                [0 , 1 , 0]  ]  )
        paths, cycles = self.sample_strong(triangle)
        self.assertEqual(convert_array(paths), [3, 3, 3])
        self.assertEqual(convert_array(cycles), [0, 0, 3])
        
    def test_strong_pes_triangle_isolated(self):
        triangle  = sp.array( [ [0 , 0 , 1, 0],
                                [1 , 0 , 0, 0],
                                [0 , 1 , 0, 0],
                                [0 , 0 , 0, 0]  ]  )
        paths, cycles = self.sample_strong(triangle)
        self.assertEqual(convert_array(paths), [4, 3, 3, 0])
        self.assertEqual(convert_array(cycles), [0, 0, 3, 0])
        
        
    def test_strong_pes_triangle_and_input(self):
        triangle  = sp.array( [ [0 , 0 , 1, 1],
                                [1 , 0 , 0, 0],
                                [0 , 1 , 0, 0],
                                [0 , 0 , 0, 0]  ]  )
        paths, cycles = self.sample_strong(triangle)
        self.assertEqual(convert_array(paths), [4, 4, 4, 1])
        self.assertEqual(convert_array(cycles), [0, 0, 3, 0])
        
        
    def test_strong_pes_rectangle(self):
        triangle  = sp.array( [ [0 , 0 , 0, 1],
                                [1 , 0 , 0, 0],
                                [0 , 1 , 0, 0],
                                [0 , 0 , 1, 0]  ]  )
        paths, cycles = self.sample_strong(triangle)
        self.assertEqual(convert_array(paths), [4, 4, 4, 4])
        self.assertEqual(convert_array(cycles), [0, 0, 0, 4])
        
    def test_nr_paths(self):
        a = sp.array(
        [[False, False, False, False, False,  True, False, False, False,
         True],
       [False, False,  True,  True, False, False, False, False, False,
        False],
       [False, False, False, False,  True, False,  True, False,  True,
         True],
       [False,  True, False, False, False, False,  True, False,  True,
        False],
       [False, False, False, False, False, False, False,  True, False,
        False],
       [False, False, False, False,  True, False, False,  True,  True,
        False],
       [False, False,  True, False,  True, False, False,  True, False,
        False],
       [False,  True, False, False, False, False,  True, False,  True,
        False],
       [False,  True, False, False, False, False, False, False, False,
         True],
       [ True,  True, False, False,  True,  True, False, False, False,
        False]], dtype=bool)
        #obtained with pure python implementation
        paths_expected = [10, 27,61, 128, 231, 355, 416, 360, 203, 63]
        cycles_expected = [0, 8, 15, 36, 70, 126, 112, 96, 45, 20]
        paths, cycles = self.sample_strong(a)
        self.assertEqual( convert_array(paths), paths_expected)
        self.assertEqual( convert_array(cycles), cycles_expected)
        

if __name__ == "__main__":
    unittest.main()
