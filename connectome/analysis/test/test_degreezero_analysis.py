import unittest
import scipy as sp
from connectome.model.network import Network
from connectome.analysis import DegreeZero


class TextConnectivityEstimator(unittest.TestCase):

    @staticmethod
    def run_from_list_of_list(list_of_list):
        net = Network(sp.array(list_of_list))
        res = DegreeZero(network=net)()
        return res["nr_outdegrees_zero"], res["nr_indegrees_zero"], res["nr_degrees_zero"]

    def test_one(self):
        arr = [[.1, .2, .3],
                [0,  0, 0],
                 [.1, .2, .3]]

        res = self.run_from_list_of_list(arr)
        self.assertEqual((0, 1, 0), res)


    def test_two(self):
        arr = [[.1, .0, .3],
                [0,  0, .2],
                [.1, .0, .3]]

        res = self.run_from_list_of_list(arr)
        self.assertEqual((1, 0, 0), res)

    def test_three(self):
        arr = [[.1, .0, .3],
               [0, .1, .2],
               [.1, .0, .3]]

        res = self.run_from_list_of_list(arr)
        self.assertEqual((0, 0, 0), res)

    def test_four(self):
        arr = [[.1, .0, .3],
               [0, .0, .0],
               [.1, .0, .3]]

        res = self.run_from_list_of_list(arr)
        self.assertEqual((1, 1, 1), res)

    def test_five(self):
        arr = [[.1, .0, .0],
               [0, .0, .0],
               [.1, .0, .0]]

        res = self.run_from_list_of_list(arr)
        self.assertEqual((2, 1, 1), res)