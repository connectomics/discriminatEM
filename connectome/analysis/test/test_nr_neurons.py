import unittest

from connectome.analysis import NrNeurons
from connectome.model.network import Network


class TestNrNeurons(unittest.TestCase):
    def test_nr_neurons(self):
        net = Network(nr_exc=3, nr_inh=2)
        result = NrNeurons(network=net)()
        self.assertEqual({"nr_exc":3, "nr_inh":2}, result)

if __name__ == "__main__":
    unittest.main()