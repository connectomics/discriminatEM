import unittest

import numpy as np

from connectome.analysis import InOutDegreeCorrelation
from connectome.model.network import Network


class TestInOutDegreeCorrelation(unittest.TestCase):
    def test_zero_network_nan(self):
        adj = [[0,0,0,0,-1],
               [0,0,0,0,-1],
               [0,0,0,0,-1],
               [0,0,0,0,0],]
        net = Network(adj, nr_exc=4, nr_inh=1)
        io_degree_correlation = InOutDegreeCorrelation(network=net)()['in_out_degree_correlation_exc']
        self.assertTrue(np.isnan(io_degree_correlation))

    def test_full_network_nan(self):
        adj = [[0,1,1,1,-1],
               [1,0,1,1,-1],
               [1,1,0,1,-1],
               [1,1,1,0,0],]
        net = Network(adj, nr_exc=4, nr_inh=1)
        io_degree_correlation = InOutDegreeCorrelation(network=net)()['in_out_degree_correlation_exc']
        self.assertTrue(np.isnan(io_degree_correlation))

    def test_finite_correlation(self):
        adj = [[0,1,.2,1,-1],
               [1,0,0,1,-1],
               [1,1,0,1,-1],
               [1,1,.2,0,0],]
        net = Network(adj, nr_exc=4, nr_inh=1)
        io_degree_correlation = InOutDegreeCorrelation(network=net)()['in_out_degree_correlation_exc']
        self.assertEqual(np.corrcoef([3,3,2,3],[3,2,3,3])[0,1], io_degree_correlation)


if __name__ == "__main__":
    unittest.main()