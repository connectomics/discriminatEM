from .baseanalysis import BaseAnalysis


class DegreeZero(BaseAnalysis):
    def analyse(self):
        binary_matrix = self.network.adjacency_matrix != 0

        outdegrees = binary_matrix.sum(axis=0)
        indegrees = binary_matrix.sum(axis=1)
        degrees = outdegrees + indegrees

        nr_outdegrees_zero = (outdegrees == 0).sum()
        nr_indegrees_zero = (indegrees == 0).sum()
        nr_degrees_zero = (degrees == 0).sum()

        return {"nr_indegrees_zero": int(nr_indegrees_zero),
                "nr_outdegrees_zero": int(nr_outdegrees_zero),
                "nr_degrees_zero": int(nr_degrees_zero)}
