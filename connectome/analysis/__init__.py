"""
Network analysis related classes.
"""

from connectome.model import Network
from .connectivity import ConnectivityEstimator
from .reciprocity import ReciprocityEstimator
from .relativecycleanalysis import RelativeCycleAnalysis
from .relativereciprocity import RelativeReciprocityEstimator
from .inoutdegreecorrelation import InOutDegreeCorrelation
from .analysisbuilder import AnalysisBuilder
from .nrneurons import NrNeurons
from .degreezero import DegreeZero


def standard_analysis(network: Network) -> dict:
    """
    Convenient standard analysis tasks

    Parameters
    ----------
    network: Network
        The network to be analysed.

    Returns
    -------
    result: dict
        Analysis result.
    """
    result = {}
    for Analysis in [ConnectivityEstimator, ReciprocityEstimator, RelativeReciprocityEstimator, InOutDegreeCorrelation]:
        result.update(Analysis(network=network)())
    return result
