from modelflow import DynamicGraph, DictionaryMerger
from collections import namedtuple

AnalysisClassInitialization = namedtuple("AnalysisClassInitialization", "cls kwargs")


class AnalysisBuilder:
    def __init__(self):
        self.analysis_list = []

    def add(self, analysis, **analysis_kwargs):
        self.analysis_list.append(AnalysisClassInitialization(analysis, analysis_kwargs))

    def get_result(self):
        result = DynamicGraph()
        result.add_inputs(["network"])
        dictionary_merger = DictionaryMerger(nr_dictionaries=len(self.analysis_list))
        for nr, analysis in enumerate(analysis.cls(**analysis.kwargs) for analysis in self.analysis_list):
            result.connect_input_to_node("network", analysis, "network")
            result.connect_node_to_node(analysis, dictionary_merger, dictionary_merger.input_name(nr))
        result.set_output(dictionary_merger)
        return result
