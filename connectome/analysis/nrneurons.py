from .baseanalysis import BaseAnalysis


class NrNeurons(BaseAnalysis):
    def analyse(self):
        return {"nr_exc": self.network.nr_exc,
                "nr_inh": self.network.nr_inh}