from modelflow import Node
from modelflow.elementarynodes import Input
from abc import abstractmethod


class _AllContainingList:
    def __contains__(self, item):
        return True

everything = _AllContainingList()


class BaseAnalysis(Node):
    network = Input()
    measures = Input(everything)

    def _filter(self, measurement: dict):
        filter = self.measures
        res = {key: value for key, value in measurement.items() if key in filter}
        return res

    @abstractmethod
    def analyse(self) -> dict:
        pass

    def run(self):
        return self._filter(self.analyse())
