from .baseanalysis import BaseAnalysis
import logging
import numpy as np
import sys


class InOutDegreeCorrelation(BaseAnalysis):
    """
    Estimates the correlation of in and out degrees of the excitatory subpopulation.

    Output keys: in_out_degree_correlation_exc
    """

    def analyse(self):
        binary_network = self.network.copy()
        binary_network.binarize()
        excitatory_out_degrees = binary_network["E", "E"].sum(axis=0)
        excitatory_in_degrees = binary_network["E", "E"].sum(axis=1)
        in_out_degree_correlation_exc = np.corrcoef(excitatory_in_degrees, excitatory_out_degrees)[0,1]

        if np.isnan(in_out_degree_correlation_exc):
            logging.warning("""\
Input- / output-degree correlation is NaN.
Unique input-degrees are {}
Unique output-degrees are {}\
""".format(np.unique(excitatory_in_degrees),
           np.unique(excitatory_out_degrees)))

        return {"in_out_degree_correlation_exc": in_out_degree_correlation_exc}
