"""
The connectome module
=====================
"""


def version():
    import git
    import datetime
    repo = git.Repo(__file__, search_parent_directories=True)
    gitstatus = repo.git.status()
    gitlog = repo.git.log(n=1)
    current_date = str(datetime.datetime.now())
    return {"status": gitstatus, "log": gitlog, "now": current_date}
