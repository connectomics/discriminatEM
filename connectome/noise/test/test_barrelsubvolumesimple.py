import unittest
from connectome.model import ER
from connectome.noise import BarrelSubvolumeSimple


class TestSubsampling(unittest.TestCase):
    def test_one(self):
        net = ER(nr_neurons=2000, inh_ratio=0.1, p_exc=0.2, p_inh=0.55)()
        subvolume = BarrelSubvolumeSimple(cube_length_um=250)
        subnet = subvolume(network=net)
        self.assertEqual(True, True)


if __name__ == "__main__":
    unittest.main()
