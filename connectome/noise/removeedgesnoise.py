from modelflow import Node, Input
import scipy as sp


class RemoveEdgesNoise(Node):
    """
    Remove and add edges uniformly.

    Parameters
    ----------

    network: :class:`Network <connectome.model.network.Network>`
        The network to which the noise is applied.

    fraction_remove: float in [0, 1]
        The fraction of edges to be removed.
    """
    network = Input()
    fraction_remove = Input()

    def run(self):
        noisy_network = self.network.copy()
        if noisy_network.adjacency_matrix.diagonal().any():
            raise Exception("No diagonal entries allowed.")
        self._remove_edges(noisy_network)
        noisy_network.binarize()
        return noisy_network

    def _remove_edges(self, noisy_network):
        """
        Diagonal (self-edges) can be remove if there are any.
        """
        targets, sources = sp.nonzero(noisy_network.adjacency_matrix)
        nr_edges = targets.size

        nr_edges_to_remove = int(nr_edges * self.fraction_remove)
        if not nr_edges_to_remove: return

        edges_to_remove = sp.random.choice(nr_edges, nr_edges_to_remove, replace=False)
        noisy_network.adjacency_matrix[targets[edges_to_remove], sources[edges_to_remove]] = 0
