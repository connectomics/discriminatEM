import numpy as np

from modelflow import Input, Node

class EdgePrecisionRecallNoise(Node):
    """
    Randomly add and remove edges according to user-specified precision and
    recall values. In contrast to RemoveAndAddEdgesNoise, the fraction of
    added and removed edges must not be identical.

    Parameters
    ----------

    network: :class:`Network <connectome.model.network.Network>`
        The network.

    precision: float in [0, 1]
        Precision of edges in output network (i.e., the fraction of edges in
        the output network that are also in the input network.)

    recall: float in [0, 1]
        Recall of edges in output network (i.e., fraction of edges in input
        network that are also in output network.)

    Written by
        Alessandro Motta <alessandro.motta@brain.mpg.de>
    """
    network = Input()
    precision = Input()
    recall = Input()

    def run(self):
        noisy_network = self.network.copy()

        if noisy_network.adjacency_matrix.diagonal().any():
            raise Exception("No diagonal entries allowed.")

        self._simulate_recall(noisy_network, self.recall)
        self._simulate_precision(noisy_network, self.precision)

        noisy_network.binarize()
        return noisy_network

    def _simulate_recall(self, noisy, recall):
        rng = np.random.default_rng()

        # Find edges
        clean = self.network
        post_ids, pre_ids = np.nonzero(clean.adjacency_matrix)
        nr_remove = int((1.0 - recall) * post_ids.size)

        # Define edge subset to remove
        to_remove = rng.choice(post_ids.size, nr_remove, replace=False)
        post_ids = post_ids[to_remove]
        pre_ids = pre_ids[to_remove]
        del to_remove

        # Remove edges
        assert np.all(noisy.adjacency_matrix[post_ids, pre_ids])
        noisy.adjacency_matrix[post_ids, pre_ids] = 0

    def _simulate_precision(self, noisy, precision):
        rng = np.random.default_rng()

        clean = self.network
        nr_neurons = clean.nr_neurons
        nr_exc = clean.nr_exc

        true_pos_nr = np.count_nonzero(noisy.adjacency_matrix)
        false_pos_nr = int(true_pos_nr * (1.0 - precision) / precision)

        # Find "disconnections"
        mask = np.logical_not(np.identity(nr_neurons, dtype=np.bool))
        mask = self.network.adjacency_matrix == 0 & mask
        post_ids, pre_ids = np.nonzero(mask)
        del mask

        # Define edge subset to introduce
        to_add = rng.choice(post_ids.size, false_pos_nr, replace=False)
        post_ids = post_ids[to_add]
        pre_ids = pre_ids[to_add]
        del to_add

        assert not np.any(clean.adjacency_matrix[post_ids, pre_ids])

        # Add edges
        mask = pre_ids < nr_exc
        noisy.adjacency_matrix[post_ids[mask], pre_ids[mask]] = +1.0

        mask = np.logical_not(mask)
        noisy.adjacency_matrix[post_ids[mask], pre_ids[mask]] = -1.0

