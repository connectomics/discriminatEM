from modelflow import Node, Input


class NoNoise(Node):
    network = Input()

    def run(self):
        return self. network