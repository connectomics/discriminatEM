import unittest
from connectome.builder import DictModifier


class TestDictModifier(unittest.TestCase):
    def test_dict_modifier(self):
        original_dict = {"1": {"11": "original"}, "2": "other stuff"}
        modification = {"1": {"11": "modified"}}
        expected = {"1": {"11": "modified"}, "2": "other stuff"}
        modifier = DictModifier(modification)
        modifier.modify(original_dict)
        self.assertEqual(expected, original_dict)

    def test_dict_modifier_dict(self):
        original_dict = {"1": {"11": "original"}, "2": {"other stuff": "here"}}
        modification = {"1": {"11": "modified"}}
        expected = {"1": {"11": "modified"}, "2": {"other stuff": "here"}}
        modifier = DictModifier(modification)
        modifier.modify(original_dict)
        self.assertEqual(expected, original_dict)


if __name__ == "__main__":
    unittest.main()
