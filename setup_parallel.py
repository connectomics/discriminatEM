from setuptools import setup

setup(name="parallel",
    version="0.1.0",
    description="Map jobs to an SGE environment",
    author="Emmanuel Klinger",
    url="http://brain.mpg.de/research/helmstaedter-department/people.html",
    author_email="emmanuel.klinger@brain.mpg.de",
    license="GPL",
    classifier=['Programming Language :: Python :: 3'],
    keywords="SGE parallel distributed map",
    packages=["parallel"],
    install_requires=["cloudpickle"])
