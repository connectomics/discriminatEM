"""
Parallel job execution
======================
"""

from parallel.util import nr_cores_available
from .execution_contexts import DefaultContext, ProfilingContext
from .sge import SGE
from .slurm import Slurm
from .util import sge_available, slurm_available

__all__ = ["nr_cores_available", "DefaultContext", "ProfilingContext", "SGE", "Slurm", "sge_available", "slurm_available"]
