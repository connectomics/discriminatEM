Connectome ABC Tasks
====================

.. automodule:: connectome.abctask

.. autoclass:: connectome.abctask.ABCConnectomeTask
   :members:

.. autoclass:: connectome.abctask.GroundTruthData
   :members:

.. autofunction:: connectome.abctask.execute_task_list