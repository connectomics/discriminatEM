RNN
---

This module implements functionality to train biological recurrent neural
networks.

.. automodule:: connectome.function.kerasextension.layers
   :members:

.. automodule:: connectome.function.kerasextension.trainingstrategies
   :members:

.. automodule:: connectome.function.kerasextension.constraints
   :members:

.. automodule:: connectome.function.kerasextension.weightinitialization
   :members:
