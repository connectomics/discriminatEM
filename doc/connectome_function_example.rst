
For example, to examine how well activity is propagated by a certain SYNIFRE
configuration import:

.. code:: python

    from connectome.function import TestSuite, GreaterThan, PropagationTask
    from connectome.model import SYN

Tests are defined as follows::

    syn_test = TestSuite(PropagationTask())
    syn_test.add_criterion("fraction_pools_activated", GreaterThan(.5))
    res = syn_test.test_model(SYN(nr_neurons=2000, inh_ratio=.1, p_exc=.15, p_inh=.5, pool_size=100))


In detail:
A test suite for the propagation task is created:

.. code:: python

    syn_test = TestSuite(PropagationTask())
    syn_test




.. parsed-literal::

    <TestSuite task=PropagationTask(spike_detection_pool_fraction=0.5, run_time=100. * msecond), criteria={}>



The "GreaterThan" criterion is used to verify that a certain fraction of pools is activated:

.. code:: python

    syn_test.add_criterion("fraction_pools_activated", GreaterThan(.5))
    syn_test




.. parsed-literal::

    <TestSuite task=PropagationTask(spike_detection_pool_fraction=0.5, run_time=100. * msecond), criteria={'fraction_pools_activated': GreaterThan(0.5)}>



A model is instantiated:

.. code:: python

    syn = SYN(nr_neurons=2000, inh_ratio=.1, p_exc=.15, p_inh=.5, pool_size=100)
    syn




.. parsed-literal::

    <SYN nr_neurons=2000, p_exc=0.15, inh_ratio=0.1, pool_size=100, p_inh=0.5>



and then tested

.. code:: python

    res = syn_test.test_model(syn)
    res["fraction_pools_activated"]



.. parsed-literal::

    TestResult(criterion=GreaterThan(0.5), score=1.0, passed=True)



The "score" output indicates the fraction of the activated pools and it is
displayed whether the test is passed or not.
In this example, all pools were activated.