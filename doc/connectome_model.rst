Connectome models
=================

This package provides connectome models.

.. autoclass:: connectome.model.bases.NetworkModelMixin
   :members:

.. autoclass:: connectome.model.network.Network
   :members:

.. autoclass:: connectome.model.block.BlockStructure
   :members:

.. autoclass:: connectome.model.er.ER
   :members:

.. autoclass:: connectome.model.exp.EXP
   :members:

.. autoclass:: connectome.model.ll.LL
   :members:

.. autoclass:: connectome.model.syn.SYN
   :members:

.. autoclass:: connectome.model.api.API
   :members:

.. autoclass:: connectome.model.sorn.SORN
   :members:

.. autoclass:: connectome.model.fever.ERFEVER
   :members:
