License
=======

This package is licensed under a MIT license. See the :ref:`license-text` below.


Author
------

This package is written by Emmanuel Klinger (emmanuel.klinger@brain.mpg.de).


.. _license-text:


License text
------------

.. literalinclude::  ../LICENSE.txt
    :language: none

