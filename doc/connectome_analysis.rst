Connectome analysis
===================

This package provides connectome analysis routines.

.. autoclass:: connectome.analysis.connectivity.ConnectivityEstimator
   :members:

.. autoclass:: connectome.analysis.inoutdegreecorrelation.InOutDegreeCorrelation
   :members:

.. autoclass:: connectome.analysis.relativecycleanalysis.RelativeCycleAnalysis
   :members:

.. autoclass:: connectome.analysis.reciprocity.ReciprocityEstimator
   :members:

.. autoclass:: connectome.analysis.relativereciprocity.RelativeReciprocityEstimator
   :members:
