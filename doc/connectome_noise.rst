Connectome noise
=================

This package implements connectome noise models.

.. module:: connectome.noise

.. autoclass:: connectome.noise.combined_noise.RemoveAddNoiseAndSubsample
   :members:

.. autoclass:: connectome.noise.barrelcutnoise.BarrelCutNoise
   :members:

.. autoclass:: connectome.noise.edgeshufflingnoise.EdgeShufflingNoise
   :members: _shuffle_subpopulation


.. autoclass:: connectome.noise.inandoutdegreepreservingnoise.InAndOutDegreePreservingNoise
   :members:


.. autoclass:: connectome.noise.outdegreepreservingnoise.OutDegreePreservingNoise
   :members:

.. autoclass:: connectome.noise.removeandaddedgesnoise.RemoveAndAddEdgesNoise
   :members:

.. autoclass:: connectome.noise.subsampling.Subsampling
   :members:





