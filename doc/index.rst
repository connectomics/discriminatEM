discriminatEM
=============


User's Guide
------------


.. toctree::
   :maxdepth: 2

   installation
   command_line_abc
   quickstart
   connectome
   license


API reference
-------------


.. toctree::
   :maxdepth: 2

   connectome_model
   connectome_analysis
   connectome_noise
   connectome_shuffling
   connectome_pes
   connectome_builder
   connectome_function
   connectome_abctask
   abc_api
   parallel_api
   rnn


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



