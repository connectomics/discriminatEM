.. _connectome-function-api-doc:

Connectome function
===================

.. automodule:: connectome.function


Tasks
-----

.. autoclass:: connectome.function.modeltest.Task

   .. automethod:: connectome.function.modeltest.Task.__call__


.. autoclass:: connectome.function.task.memory.MemoryTask
   :members:

.. autoclass:: connectome.function.task.texture.TextureTask
   :members:

.. autoclass:: connectome.function.task.tuning.TuningTask
   :members:

.. autoclass:: connectome.function.task.unsynchronized.UnsynchronizedActivityTask
   :members:

.. autoclass:: connectome.function.task.propagation.task.PropagationTask
   :members:



Criteria
--------

.. autoclass:: connectome.function.modeltest.Criterion

   .. automethod:: connectome.function.modeltest.Criterion.__call__

.. automodule:: connectome.function.criterion
   :members:


Test runners
------------

.. autoclass:: connectome.function.modeltest.TestSuite
   :members:


Test results
------------

.. autoclass:: connectome.function.modeltest.TestSuiteResultList
   :members:

.. autoclass:: connectome.function.modeltest.TestSuiteResult
   :members:

.. autoclass:: connectome.function.modeltest.TestResult
   :members: