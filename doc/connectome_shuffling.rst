Network shuffling
=================


.. autofunction:: connectome.shuffling.uniform_degree_preserving_shuffling.shuffle_graph
