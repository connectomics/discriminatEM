The connectome package
======================

In the following the different parts of software package are described in more
depth than in the quick start tutorial.
These parts include network generation, network analysis, noise models,
model composition and pipeline generation, functional model checking and
ABC inference.

Creating networks - Sampling from stochastic network models
-----------------------------------------------------------

Network models are defined within the `connectome.model` package.
For example, the FEVER model is simulated with::

    from connectome.model import FEVER
    fever = FEVER(nr_neurons=2000, inh_ratio=.2, feverization_ratio=.5,
                  feature_space_dimension=100)
    network = fever(p_exc=.2, p_inh=.5)

or alternatively::

    fever = FEVER(nr_neurons=2000, inh_tatio=.2, p_exc=.2, p_inh=.5,
                  feverization_ratio=.5, feature_space_dimension=100)
    network = fever()

Any argument can be either passed at object initialization and is then stored or can be passed when the object is called.
Any argument provided once is stored and can be omitted on subsequent calls if no change is required.


The provided models are

* :class:`Erdős–Rényi - echo state network (ER-ESN) <connectome.model.er.ER>`
* :class:`Exponentially decaying connectivity - liquid state machine (EXP-LSM) <connectome.model.exp.EXP>`
* :class:`Layered network (LAYERED) <connectome.model.ll.LL>`
* :class:`Synfire chain (SYNFIRE) <connectome.model.syn.SYN>`
* :class:`Feature vector recombination network (FEVER) <connectome.model.fever.ERFEVER>`
* :class:`Antiphase inhibition (API) <connectome.model.api.API>`
* :class:`Spike timing dependent plasticity - self-organizing recurrent neural network (STDP-SORN) <connectome.model.sorn.SORN>`

Please note the following correspondences between the class names used in this
code package and the model acronyms used in the manuscript (class name ➜ acronym):

* ER ➜ ER-ESN,
* EXP ➜ EXP-LSM,
* LL ➜ LAYERED,
* SYN ➜ SYNFIRE,
* ERFEVER ➜ FEVER,
* API ➜ API,
* SORN ➜ STDP-SORN.




Networks
--------

A network sample is represented by an instance
of the :class:`Network <connectome.model.network.Network>` class.
This class represents a biological neural network and is a thin wrapper around
a Numpy 2D array with additional methods and properties relevant
for biological neural networks. These properties include the number
of excitatory neurons `Network.nr_exc` and the number of inhibitory neurons `Network.nr_inh`.
It also supports indexing in terms of cell type. For example::

    network["E", "I"]

returns a view on the block of the matrix projecting from the excitatory population to the inhibitory population.
In addition, this class provides methods for relabeling and removing neurons.
Details are found in the API documentation: :class:`Network <connectome.model.network.Network>`.


Network analysis
----------------

The `connectome.analysis` module implements functionality for the analysis of connectomes.
All the analysis classes accept a single input called `network` and return a dictionary.
For example, to calculate the excitatory connectivity, execute::

    from connectome.model import ER
    from connectome.analysis import ConnectivityEstimator

    er = ER(nr_exc=2000, inh_ratio=.1, p_exc=.2, p_inh=.5)
    network = er()
    connectivity_estimator = ConnectivityEstimator()
    analysis_result = connectivity_estimator(network=network)
    excitatory_connectivity = analysis_result["p_ee"]



Noise in networks
-----------------

Noise models are defined in the `connectome.noise` package. Noise objects modify networks. For example::

    from connectome.model import LL
    from connectome.noise import Subsampling

    network = LL(nr_neurons=2000, inh_ratio=.1, p_exc=.2, p_inh=.5, reciprocity_exc=.2)()
    noisy_network = Subsmapling(network=network, subsampling_fraction=.7)()

Produces a network `noisy_network` in which 30% of the original neurons are randomly removed.
This network can be analyzed or further noise can be applied to it.



Monte Carlo sampling based analysis
-----------------------------------

The `connectome.pes` package provides efficient Cython implementations of path sampling algorithms. Weak path enumeration sampling,
provided by :func:`weak_edge_pes <connectome.pes.weak_path_enumeration_sampling.weak_edge_pes>` does not take into account
the direction of connections. Strong path enumeration sampling,
provided by :func:`pes <connectome.pes.strong_path_enumeration_sampling.pes>`, takes the direction of edges into account.

Path sampling provides an alternative way to quantify the number of cycles in a network. The cycles analysis implemented in
:class:`RelativeCycleAnalysis <connectome.analysis.relativecycleanalysis.RelativeCycleAnalysis>` is a measure of dynamic cycles:
an edge can occur in a dynamic cycle multiple times. The path sampling algorithms measure static cycles in which each
edge occurs at most once.


Building composite models
-------------------------

A composite model can be build as pipeline of different processing steps:

1. network sampling,
2. noise application,
3. network analysis.

.. image:: model_pipeline.svg

Functionality for the assembly of such pipelines is implemented in the
`connectome.builder` module.


Definition of models pipelines with priors, noise and analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The necessary parts to define a pipeline can be stored in a JSON file.
The first level is structured into three parts: "models", "noise" and "analysis":

.. code-block:: text

   {
      "models": ...
      "noise": ...
      "analysis": ...
   }

The "models" section is further subdivided into "joint_parameters" and "model_specific_parameters".
The "joint_parameters" are the ones shared by all models, the "model_specific_parameters" are the ones
which apply only to a specific model. The "model_specific_parameters" contain one subsection for each model --
if that model has additional parameters or not.
In these subsections, the prior distributions can be defined (if applicable). There
are two ways to define a prior:

1. a fixed delta point prior
2. a distribution

A delta point prior is defined by providing the point as number:

.. code-block:: json

  {
    "pool_size": 80
   }

The above defines the parameter "pool_size" to be fixed at 80.
A distribution is defined by indicating the distribution name and its shape parameters:

.. code-block:: json

       {
        "pool_size": {
          "type": "randint",
          "args": [
            80,
            300
          ]
        }

The above defines "pool_size" to be uniformly distributed on the set :math:`\{80, 81, 82, \ldots, 298, 299\}` [#not_last]_.
Distributions from the `scipy.stats` package are supported. The "type" determines the distribution, "args" its arguments.
Additionally, a "kwargs" key might also be present. The content is then passed as keyword argument to the `scipy.stats`
distribution. The definition of noise is analogous to the definition of models.

A number of analysis classes can be defined to produce the output of the pipeline. Every class which occurs
in the analysis section produces its output. These are then combined. A full example of a model definitions JSON
file could look like the one below:


.. literalinclude:: ../connectome/model/definitions.json
   :language: json


Building models and sampling using model definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Having defined the priors, the noise and the analysis tasks the `connectome.builder` package can be used
to sample and analyse models (see :doc:`connectome_builder`.).
An example script is provided below.


.. literalinclude:: ../scripts/sample.py


Functional model testing
------------------------

Classes for functional model testing are implemented in the ``connectome.function`` package (see :doc:`connectome_function`).

.. include:: connectome_function_example.rst

It is also possible to test many models simultaneously, possibly on a distributed execution engine.
This can be achieved using the ``enqueue_model`` and ``execute_enqueued_models`` methods of the
:class:`TestSuite <connectome.function.modeltest.TestSuite>`.
Additionally, the ``TestSuite.mapper`` class attribute can be set for distributed execution or a mapper can be passed to
``execute_enqueued_models``.

.. [#not_last]  The last number is not included.


ABC with network models
-----------------------

For ABC tasks with networks, the
Facade class :class:`ABCConnectomeTask <connectome.abctask.ABCConnectomeTask>` is provided and recommended.
This class encapsulates inference tasks.
A list of such tasks can be executed with the :func:`execute_task_list <connectome.abctask.execute_task_list>`
function.


