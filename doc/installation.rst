Installation
============




Preparation
-----------

This package requires Python 3.6 or later.
Cython and Numpy should be installed before installation of this package,
for example via pip::

 pip install numpy
 pip install cython

These packages are required for a successful installation.
If the Anaconda Python distribution is used,
Numpy and Cython should be already included.


PIP
---

The package is installed from the provided archive via pip::

   pip install discriminatEM-0.1.3.tar.gz


Installation via git will be made available upon publication.


Windows
~~~~~~~

Visual Studio 2015 is required in order to build this package from source

.. warning::

    It is crucial to mark the ticks for the C++ language and the Visual Studio Common tools
    in the Visual Studio 2015 installer. Otherwise an error message
    "vcvarsall.bat not found" will be seen.




Running the unit- and integration tests
---------------------------------------

To run the unit- and integration tests, change into the source code root directory.
This is the directory containing the setup.py file.
Then install the package requirements::

    pip install -r requirements.txt


Compile the binary extensions in-place::

    python setup.py build_ext --inplace

and execute the test suits::

    python -m unittest

This discovers the available tests and runs them.


Some of the tests involve stochastic model sampling and computationally expensive procedures.
These are disabled by default.
To also enable these long running tests, set the environment variable ``RUN_LONG_TESTS=true``.


.. note::

   Tests involving stochastic network generation
   may fail from time to time although there is no problem.
   If an error states that a quantity should be less than some value, but the
   actually obtained number is slightly above, this can be safely ignored.
