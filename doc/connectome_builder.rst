.. _connectome-builder-api-doc:

Connectome builder
==================

.. automodule:: connectome.builder

.. autoclass:: connectome.builder.pipelinebuilder.PipelineBuilder
   :members:

.. autofunction:: connectome.builder.pipelinebuilder.concatenate_model_noise_analysis


.. autoclass:: connectome.builder.sampler.ModelWithPrior
   :members:


.. autoclass:: connectome.builder.sampler.TimedModelWithPriorSample
   :members:


.. autoclass:: connectome.builder.persistence.InMemoryStorer
   :members:

.. autoclass:: connectome.builder.persistence.DistributedFileStorer
   :members:

.. autoclass:: connectome.builder.sampler.Sampler
   :members:

