#!/usr/bin/env bash
module load cuda/9.0
module load cudnn/7.4-cuda-9.0

# Manually set up Conda. See
# https://github.com/conda/conda/issues/7980#issuecomment-472648567
conda_base=$(conda info --base)
source "${conda_base}/etc/profile.d/conda.sh"
conda activate klinger

# Run Python script and forward command line arguments
python model.py $@
