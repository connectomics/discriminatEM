% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

info = Util.runInfo();
Util.showRunInfo(info);

%% Panel 4c
clear cur*;

curX = linspace(0, 1, 101);
curY = betapdf(curX, 2, 10);

curFig = figure();
curAx = axes(curFig);

plot(curAx, curX, curY, 'Color', 'black')
xlim(curAx, [0, 1]);
ylim(curAx, [0, 5]);

Figure.config(curFig, info);
