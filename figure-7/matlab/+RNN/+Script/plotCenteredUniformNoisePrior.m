% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
centers = [0.3, 0.6, 0.9];
rangeFromCenter = 0.04;

height = 1 / (2 * rangeFromCenter);
maxY = 10 * ceil(height / 10);

info = Util.runInfo();
Util.showRunInfo(info);

%% Plot
fig = figure();
ax = axes(fig);
hold(ax, 'on');

colors = linspace(1, 0, 1 + numel(centers) + 1);
colors = reshape(colors((1 + 1):(end - 1)), [], 1);
colors = repmat(colors, 1, 3);

for idx = 1:numel(centers)
    center = centers(idx);
    color = colors(idx, :);
    
    min = center - rangeFromCenter;
    max = center + rangeFromCenter;
    height = 1 / (max - min);
    
    plot(ax, ...
        [0, min, min, max, max, 1], ...
        [0, 0, height, height, 0, 0], ...
        'Color', color, 'LineWidth', 2);
    plot(ax, [center, center], [0, maxY], 'Color', color);
end

xlim(ax, [0, 1]);
xticks(ax, 0:0.1:1);

ax.XTickLabel(:) = {''};
labels = [0, centers, 1];
ax.XTickLabel(round(1 + labels / 0.1)) = arrayfun( ...
    @(l) sprintf('%g', l), labels, 'UniformOutput', false);

ylim(ax, [0, maxY]);
yticks(ax, 0:10:20);

Figure.config(fig, info);
fig.Position(3:4) = [300, 100];
