% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
xlsFile = fullfile( ...
    '/mnt', 'mpibr', 'data', 'Personal', 'mottaa', ...
    'Klinger', '2019-10-28-Measure-Distributions', ...
    'description_noisefree_v1.xlsx');

plots = struct;
plots(1).varName = 'eeRelativeReciprocity'; plots(1).xLim = [0, 4];
plots(2).varName = 'eiRelativeReciprocity'; plots(2).xLim = [0, 2];
plots(3).varName = 'relativeCycles5'; plots(3).xLim = [0, 10];
plots(4).varName = 'excInOutDegreeCorrelation'; plots(4).xLim = [-1, +1];
yScale = 'log'; yLim = 10 .^ [-1, +1];

info = Util.runInfo();
Util.showRunInfo(info);

%% Load data
clear cur*;

dataT = RNN.loadModelMeasures(xlsFile);
modelNames = dataT.Properties.RowNames;

%% Reproduce plots form manuscript
clear cur*;

curFig = figure();

for curPlotIdx = 1:numel(plots)
    curPlot = plots(curPlotIdx);
    
    curMeans = dataT{:, sprintf('%sMean', curPlot.varName)};
    curStds = dataT{:, sprintf('%sStd', curPlot.varName)};
    
    curX = linspace(curPlot.xLim(1), curPlot.xLim(2), 101);
    curY = normpdf(curX, curMeans, curStds);
    
    curAx = subplot(1, numel(plots), curPlotIdx);
    plot(curAx, curX, curY, 'LineWidth', 2);
    
    xlim(curAx, curPlot.xLim);
    xticks(curAx, union(curPlot.xLim, [0, 1]));
    xlabel(curAx, curPlot.varName);
    
    curAx.YScale = yScale;
    ylim(curAx, yLim);
    yticks(curAx, yLim);
end

curFig.Position(3:4) = [800, 165];
Figure.config(curFig, info);

curAxes = reshape(curFig.Children, 1, []);
curDeltaX = curAxes(end).Position(1) - 0.05;
for curAx = curAxes; curAx.Position(1) = curAx.Position(1) - curDeltaX; end
for curAx = curAxes; curAx.Position([2, 4]) = [0.275, 0.45]; end

curAx = curAxes(1);
curAxPos = curAx.Position;
curLeg = legend(curAx, modelNames, 'Location', 'EastOutside');
curAx.Position = curAxPos;
curLeg.Position(2) = (1 - curLeg.Position(4)) / 2;
curLeg.Box = 'off';
