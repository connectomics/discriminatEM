function [dataT, rawData] = loadModelMeasures(xlsFile, rawXlsDir)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    rawData = struct;
    
    %% Configuration
    % NOTE(amotta): The `xlsread` function seems unable to read non-numeric
    % data, such as variable and column names. Hence the hard-coded values.
    modelNames = { ...
        'API', 'ER', 'EXP', 'FEVER', 'LL', 'SORN', 'SYN'};
    varNamesSub = { ...
        'Count', 'Mean', 'Std', 'Min', ...
        '25thPerc', 'Median', '75thPerc', 'Max'};
    varNamesSuper = { ...
        'ieReciprocity', 'nrExc', 'eiPropability', 'iiReciprocity', ...
        'ieRelativeReciprocity', 'eiReciprocity', 'eeProbability', ...
        'eiRelativeReciprocity', 'iiRelativeReciprocity', ...
        'ieProbability', 'iiProbability', 'eeRelativeReciprocity', ...
        'eeReciprocity', 'excInOutDegreeCorrelation', ...
        'relativeCycles5', 'inhibitoryCount', 'neuronCount', ...
        'inhibitoryFraction'};

    %% Generate variable names
    clear cur*;

    curFlatten = @(cell) cat(2, cell{:});
    curAllPairs = @(a, b) curFlatten(arrayfun( ...
        @(aa) strcat(aa, b), a, 'UniformOutput', false));
    varNames = curAllPairs(varNamesSuper, varNamesSub);

    %% Load data from Excel file
    dataT = xlsread(xlsFile, 1, 'B4:EO10', 'basic');
    assert(isequal(size(dataT, 1), numel(modelNames)));
    assert(isequal(size(dataT, 2), numel(varNames)));

    dataT = array2table(dataT, ...
        'RowNames', modelNames(:), ...
        'VariableNames', varNames(:));
    
    %% Load raw data from Excel file
    if ~exist('rawXlsDir', 'var') || isempty(rawXlsDir); return; end
    
    for curIdx = 1:numel(modelNames)
        curModelName = lower(modelNames{curIdx});
        curRawXlsFile = sprintf('%s.xlsx', curModelName);
        curRawXlsFile = fullfile(rawXlsDir, curRawXlsFile);
        
        curDataT = xlsread(curRawXlsFile, 1, 'C2:T51', 'basic');
        assert(isequal(size(curDataT, 2), numel(varNamesSuper)));
        
        curDataT = array2table(curDataT, ...
            'VariableNames', varNamesSuper(:));
        rawData.(curModelName) = curDataT;
    end
end
