# Connectomic separability of sparse recurrent neural networks trained on different tasks (Figure 7)

This directory contains code and data related to the connectomic
separability of sparse recurrent neural networks (RNNs) trained on
different tasks (Figure 7).

The RNNs were trained with
* `python/texture/train.sh` and
* `python/memory/train.sh`, respectively.

The connectome statistics and performance metrics were then extracted
from the TensorFlow checkpoints to CSV files with
* `python/utils/extract_connectome_measures.py` and
* `python/utils/extract_tensorflow_summaries.py`.

These data were then analyzed with
* `matlab/+RNN/+Script/plotMeasures.m`.


## Provenience
These directories were extraced from  
git@gitlab.mpcdf.mpg.de:connectomics/amotta.git  
commit bbf4b464b6966a5d51fd5c6e7ccb0700cc2071fc


