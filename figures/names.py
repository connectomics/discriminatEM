__author__ = 'emmanuel'

noise_type_rename = {"ShuffleEdgesEndsNoise": "Fraction outgoing edges reshuffled",
                     "UnbiasedDegreePreservingShuffling": "Fraction canonical moves",
                     "RemoveAndAddEdgesNoise": "Fraction removed and added"}