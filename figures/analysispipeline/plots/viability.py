import os

import luigi
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from figures import OutputFolder, style
from figures.analysispipeline.auxiliary import plot
from figures.analysispipeline.data import DataBase
from figures.analysispipeline.datatypes import DatasetTask, JSON


class VerificationData(DatasetTask):
    def requires(self):
        return {"results": DataBase(self.dataset + ":test_results.json"),
                "suites": DataBase(self.dataset + ":test_suites.json")}

    def output(self):
        return {"results": JSON(self.input()["results"].fn),
                "suites": JSON(self.input()["suites"].fn)}

    def run(self):
        pass


