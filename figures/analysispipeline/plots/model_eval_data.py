from figures import  memory


@memory.cache
def model_eval_data(db, model_checkpoints, id):
    from connectome.function.task.texture import TextureModelAnalyser
    evaluation = TextureModelAnalyser(db, model_checkpoints, id)
    _, y_gt = evaluation.get_test_data()
    traces = evaluation.predict()
    conf_mat = evaluation.confusion_matrix()
    y_pred = evaluation.class_prediction()
    return traces, y_gt, conf_mat, evaluation.nr_layers_, evaluation.comment_, y_pred