"""
Scaling behavior
----------------

Might be linear, because

* connectivity does not scale stay constant
* number of connections scales approximately linearly with network size, b/c
  connectivity drops
* e.g. a barrel field with n barrels is just n times the same network
  simulation. so linear scaling here

"""

from connectome.model import ER, EXP, ERFEVER, LL, API, SORN, SYN


common = dict(inh_ratio=.1,
              p_exc=.2,
              p_inh=.6)


def er(n):
    return ER(nr_exc=n, **common)()
