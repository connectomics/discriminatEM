from figures.analysispipeline.config import config
config.monkey_patch()

import argparse

parser = argparse.ArgumentParser(description="Make plots for paper")
parser.add_argument("--force-replot", action='store_true')
args = parser.parse_args()


import luigi


from figures.analysispipeline.plots.parameter_distributions import (LLAdjacencyMatrix, MeasureDistribution)
from figures.analysispipeline.plots import (PlotPosteriorDistributionMatrix, OneDPosteriors,
                                            Priors)
from figures.analysispipeline.plots.originalvsmodified import APIOrginalVsAdaptedPlot, FEVEROriginalVsModifiedPLot, LLROverP, ERConnectivityReciprocityScatter
from figures.analysispipeline.sorn import SORNOriginalVsModified, SORNROverP
from figures.analysispipeline.plots.illustrations import SynConnectivityReciprocityNrPools, \
    ExpConnectivityReciprocityScatter, SubsamplingMatrix, LLMathPlots, SYNRPHeatmap, APIProbabilityOverCorrelation, \
    FeatureVectorCorrelationHistogram, SORNOverTime, SynPiOverInhPoolSize, SORNPOverTimeSubTimeStep, APINBinomWeightDistribution, \
    FEVERLambdaFeatureSpaceDimensionConnRec, TaskTexture, SynActivityRaster, APITuning, TextureTraces, SORNExamples, SORNSaneActivity, \
    FEVERMemory, APIFeatureSpaceDimension, GrayScaleTextures, FEVERNetworkRepresentationPolar, Colors, SORNZeroAndInfinitTimeWeightDistribution, \
    Names, SBMPlot, FEVEROriginalVsModifiedFEVERRuleSatisfactionPLot, ReconstructionTimes, SYNPoolPropagationSummary, ABCIllustration, SynapseNumbers,\
    MeanABCTime, SYNGeGiInterpolation, APIMemory, FEVERTuning, FEVERRepresentation, APIRepresentation
from figures.analysispipeline.plots.poverd import ExpConnectivityOverDistance, ERConnectivityOverDistance
from figures.analysispipeline.plots.posterior import ModifiedExp, ModifiedSORN, CombinedPosteriorDistributionMatrix, AverageConfusion, PosteriorNoParameterGrouping
import subprocess


if args.force_replot:
    from figures.analysispipeline.auxiliary import ReplotMixin
    ReplotMixin.force_replot = True

one_d_posterior_models = ["LL", "ER"]

print("Syncronizing GABA data directory.")
import sys
#res = subprocess.run(["sh", "/home/emmanuel/WritingProjects/NetworkanalysisPaper/figures/sync_data.sh"],
#                     stdout=sys.stdout, stderr=sys.stderr)
print("Syncronizing GABA data directory: DONE!")




tasks = [
    APIOrginalVsAdaptedPlot(),
    APIProbabilityOverCorrelation(),
    APITuning(),
    APIFeatureSpaceDimension(),
    APINBinomWeightDistribution(),
    APIMemory(),
    APIRepresentation(),

    FEVEROriginalVsModifiedPLot(),
    FEVEROriginalVsModifiedFEVERRuleSatisfactionPLot(),
    FEVERLambdaFeatureSpaceDimensionConnRec(),
    FEVERMemory(),
    FEVERTuning(),
    FEVERNetworkRepresentationPolar(),
    FEVERRepresentation(),


    SORNOriginalVsModified(),
    SORNROverP(),
    SORNOverTime(),
    SORNExamples(),
    SORNPOverTimeSubTimeStep(),
    SORNSaneActivity(),
    SORNZeroAndInfinitTimeWeightDistribution(),

    LLROverP(),
    LLAdjacencyMatrix(),
    LLMathPlots(),

    SynConnectivityReciprocityNrPools(),
    SynPiOverInhPoolSize(),
    SynActivityRaster(),
    SYNRPHeatmap(),
    SYNPoolPropagationSummary(),
    SYNGeGiInterpolation(),

    ERConnectivityReciprocityScatter(),
    ERConnectivityOverDistance(),

    ExpConnectivityReciprocityScatter(),
    ExpConnectivityOverDistance(),

    Colors(),

    TaskTexture(),
    TextureTraces(),
    GrayScaleTextures(),

    SubsamplingMatrix(),
    Priors(),
    FeatureVectorCorrelationHistogram(),
    PlotPosteriorDistributionMatrix("simulations:runs:2016:05:25:2016_05_25_17_36_07:2016-05-25-no-noise.db"),
    PlotPosteriorDistributionMatrix("simulations:runs:2016:05:25:2016_05_25_18_29_12:2016-05-25-no-noise-subs-0.3.db"),
    PlotPosteriorDistributionMatrix("simulations:runs:2016:06:02:2016_06_02_17_12_28:2016-06-02-subs-1-noise-0.15-beta.db"),
    PlotPosteriorDistributionMatrix("simulations:runs:2016:06:06:2016_06_06_09_29_09:2016-06-06-subs-1-noise-0.15-noiselessprior.db"),
    PlotPosteriorDistributionMatrix(dataset="simulations:runs:2016:sweep.db", only_matrix=False),
    PlotPosteriorDistributionMatrix("simulations:runs:2016:08:23:2016_08_23_16_04_00:center_noise_prior_around_real_noise.db"),
    PlotPosteriorDistributionMatrix("simulations:runs:2017:07:14:2017_07_14_16_24_22:noise_0.2_to_0.3_subsampling_0.1_to_0.5.db"),
    PlotPosteriorDistributionMatrix("2017-09-06-sweep-complete-noiseprior-beta-2-10.db"),
    PosteriorNoParameterGrouping("2018-04-27-removeedges-noise-0.15"),
    PosteriorNoParameterGrouping("2018-04-27-removeedges-noise-0.8"),
    PosteriorNoParameterGrouping("2018-05-05-addedges-noise-0.15"),
    PosteriorNoParameterGrouping("2018-05-05-addedges-noise-0.8"),
    PosteriorNoParameterGrouping("2018-05-05-removeaddbiased-noise-1"),
    PosteriorNoParameterGrouping("2018-05-05-removeaddbiased-noise-0.15"),
    PosteriorNoParameterGrouping("2018-05-20-cutfrommiddle-noise-0.15"),
    AverageConfusion(runs=["2016-05-25-no-noise.db",
                           "2017-08-01-confmat-noise-0-subs-1-noiseprior-0.db",
                           "2017-08-01-repetition-2-confmat-noise-0-subs-1-noiseprior-0.db"],
                     output_folder="noise-0-subs-1-noiseprior-0.db"),
    AverageConfusion(runs=["2016-06-06-subs-1-noise-0.15-noiselessprior.db",
                           "2017-08-01-confmat-noise-0.15-subs-1-noiseprior-0.db",
                           "2017-08-01-repetition-2-confmat-noise-0.15-subs-1-noiseprior-0.db"],
                     output_folder="noise-0.15-subs-1-noiseprior-0"),
    AverageConfusion(runs=["2016-06-02-subs-1-noise-0.15-beta.db",
                           "2017-08-01-confmat-noise-0.15-subs-1-noiseprior-beta.db",
                           "2017-08-01-repetition-2-confmat-noise-0.15-subs-1-noiseprior-beta.db"],
                     output_folder="noise-0.15-subs-1-noiseprior-beta-2-10"),
    CombinedPosteriorDistributionMatrix(["simulations:runs:2017:07:14:2017_07_14_16_24_22:noise_0.2_to_0.3_subsampling_0.1_to_0.5.db",
                                         "2017-08-17-sweep-noise-0.1-0.2-0.25-0.3-0.5-0.9-subs-0.9-noiseprior-beta-2-10.db",
                                         "2017-08-17-sweep-noise-0.1-0.5-0.9-subs-0.1-noiseprior-beta-2-10.db",
                                         "2017-08-17-sweep-noise-0.1-0.5-0.9-subs-0.5-noiseprior-beta-2-10.db",
                                         "simulations:runs:2016:sweep.db",
                                         "2017-09-06-sweep-complete-noiseprior-beta-2-10.db"
                                         ]),
    OneDPosteriors("simulations:runs:2016:05:25:2016_05_25_17_36_07:2016-05-25-no-noise.db",
                   posterior_models=one_d_posterior_models),
    OneDPosteriors("simulations:runs:2016:05:25:2016_05_25_18_29_12:2016-05-25-no-noise-subs-0.3.db",
                    posterior_models=one_d_posterior_models),
    OneDPosteriors("simulations:runs:2016:06:06:2016_06_06_09_29_09:2016-06-06-subs-1-noise-0.15-noiselessprior.db",
                   posterior_models=one_d_posterior_models),
    OneDPosteriors("simulations:runs:2016:06:02:2016_06_02_17_12_28:2016-06-02-subs-1-noise-0.15-beta.db",
         posterior_models=one_d_posterior_models),
    MeasureDistribution("simulations:runs:2016:06:28:2016_06_28_15_56_50:samples_noisefree.db"),
    MeasureDistribution("simulations:runs:2016:06:28:2016_06_28_15_34_55:samples.db"),
    ModifiedExp("simulations:runs:2016:07:12:2016_07_12_13_12_06:modified_exp.db"),
    ModifiedSORN("simulations:runs:2017:02:10:2018_02_10_00_00_00:modified_sorn.db"),
    #Names(),
    SBMPlot(),
    ReconstructionTimes(),
    SynapseNumbers(),
    ABCIllustration(),
    MeanABCTime("simulations:runs:2016:05:25:2016_05_25_17_36_07:2016-05-25-no-noise.db"),
]


force_replot = []

for obj in force_replot:
    obj.force_replot = True


class All(luigi.Task):
    def complete(self):
        return all(task.complete() for task in self.requires())

    def requires(self):
        return tasks

    def output(self):
        return [task.output() for task in tasks]


if __name__ == "__main__":
    luigi.build([All()], local_scheduler=True)

