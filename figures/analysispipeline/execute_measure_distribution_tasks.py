import argparse
parser = argparse.ArgumentParser(description="Make plots for paper")
parser.add_argument("--force-replot", action='store_true')
parser.add_argument("--database-cache-path")
parser.add_argument("--plot-path")
args = parser.parse_args()

import os
os.environ.update({'PLOT_PATH': args.plot_path,
                   'DATABASE_CACHE_PATH': args.database_cache_path})

from figures.analysispipeline.config import config
config.monkey_patch()

import luigi

from figures.analysispipeline.plots.parameter_distributions import MeasureDistribution

if args.force_replot:
    from figures.analysispipeline.auxiliary import ReplotMixin
    ReplotMixin.force_replot = True

tasks = [
    MeasureDistribution("simulations:runs:2016:06:28:2016_06_28_15_56_50:samples_noisefree.db"),
    MeasureDistribution("simulations:runs:2016:06:28:2016_06_28_15_34_55:samples.db"),
]

class All(luigi.Task):
    def complete(self):
        return all(task.complete() for task in self.requires())

    def requires(self):
        return tasks

    def output(self):
        return [task.output() for task in tasks]


if __name__ == "__main__":
    luigi.build([All()], local_scheduler=True)

