import os
import luigi
import tempfile
import warnings


class Notice(luigi.Task):
    path = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(os.path.join(str(self.path), "IMPORTANT_NOTICE.txt"))

    def run(self):
        try:
            os.makedirs(str(self.path))
        except FileExistsError:
            pass
        with open(self.output().fn, "w") as f:
            f.write("IMPORTANT NOTICE\n"
                    "----------------\n"
                    "All content in this folder is programatically created.\n"
                    "DO NOT MODIFY ANY CONTENT BY HAND!!!")


try:
    PLOT_PATH = os.environ["PLOT_PATH"]
except KeyError:
    PLOT_PATH = tempfile.gettempdir()
    warnings.warn("Environment variable PLOT_PATH not set. Using {} instead".format(PLOT_PATH))



class OutputFolder(luigi.Task):
    path = luigi.Parameter(os.environ["PLOT_PATH"])

    def requires(self):
        return Notice(self.path)

    def output(self):
        return luigi.LocalTarget(self.path)

    def run(self):
        # everything happens in notice
        pass
