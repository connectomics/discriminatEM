import json


def load_model_definitions_json():
    with open("/home/emmanuel/PythonCode/connectome/model/definitions.json") as f:
        parameters = json.load(f)
    models = parameters["models"]
    return models

parameters = [
    ("p_exc", "p_{exc}", "Excitatory connectivity"),
    ("p_inh", "p_{inh}", "Inhibitory connectivity"),
    ("nr_neurons", "n", "Number of neurons"),
    ("inh_ratio", "f_i", "Fraction inhibitory neurons")
]
