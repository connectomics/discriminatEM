from jinja2 import Template
import os
import subprocess


def make_table(*args, **kwargs):
    with open(os.path.join(os.path.dirname(__file__), "table_template.html")) as f:
        html_template = f.read()
    rendered = Template(html_template).render(*args, **kwargs)

    html_file = "/home/emmanuel/tmp/rendred.html"

    with open(html_file, "wt") as f:
        f.write(rendered)

    subprocess.run(["pandoc", "-i", html_file, "-f", "html+tex_math_dollars", "--mathml", "-s",
                    "-o", os.path.splitext(html_file)[0] + "2.html"])


if __name__ == "__main__":
    make_table()
