#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  8 16:25:22 2017

@author: emmanuel
"""
import jinja2
from figures.analysispipeline.config import config
names = config.acronym_dict()


template = jinja2.Template(
    """
    % WARNING THIS FILE IS AUTOMATICALLY CREATED
    % By the figures/names_to_tex.py script.
    %
    % ANY CHANGES TO THIS FILE YOU MAKE BY HAND ARE LIKELY OVERWRITTEN
    % DEFINE NEW GLOSSARY ENTRIES IN THE main.tex FILE OR ANOTHER FILE
    
    \\newignoredglossary{abbrignored}
    
    {% for key, short, long in names %}\
      \\newacronym{{ "{" }}{{ key }}{{ "}{" }}{{ short }}{{ "}{" }}{{ long }}{{ "}" }}
      {% if  short == ""  %}
         \\glsmoveentry{{ "{" }}{{key}}{{ "}" }}{{ "{" }}abbrignored{{ "}" }}
      {% endif %}
    {% endfor %}
    """
)


def sanitize_math(s: str):
    if s.startswith("$") and s.endswith("$"):
        return f"\\ensuremath{{{s[1:-1]}}}"
    return s


def sanitize_key(s: str):
    return s.replace("_", "")


def names_to_tex_gls():
    to_print = [(sanitize_key(key),
                 sanitize_math(short),
                 sanitize_math(long))
                for key, (long, short) in names.items()]
    return template.render(names=to_print)


def names_to_tex_file(s: str):
    with open(s, "w") as f:
        f.write(names_to_tex_gls())


if __name__ == "__main__":
    import sys
    names_to_tex_file(sys.argv[1])
