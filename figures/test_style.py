import unittest
from figures.style import Tex


class TextTex(unittest.TestCase):
    def test_tex_format(self):
        s = "{}".format(Tex("a"))
        self.assertEqual("$a$", s)

    def test_add(self):
        s = "{}".format(Tex("a") + Tex("b"))
        self.assertEqual("$ab$", s)

    def test_from_iterable(self):
        s = "{}".format(Tex("a", Tex("b"), "c"))
        self.assertEqual("$abc$", s)

    def test_sum(self):
        lst = [Tex("a"), Tex("b"), Tex("c")]
        s = "{}".format(sum(lst, Tex()))
        self.assertEqual("$abc$", s)

if __name__ == "__main__":
    unittest.main()