from figures.analysispipeline.config import config


def single_color(c, nr):
    r, g, b = c
    return "\\definecolor{c" + str(nr) + "}{RGB}{" + f"{r},{g},{b}" + "}"


def color_scheme_to_tex(config):
    palette = config.palette
    color_names = [single_color(c, n) for n, c in enumerate(palette)]
    return "\n".join(color_names) + "\n"

if __name__ == "__main__":
    import sys
    with open(sys.argv[1], "w") as f:
        f.write(color_scheme_to_tex(config))
