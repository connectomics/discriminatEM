from figures.analysispipeline import config
from joblib import Memory
from .analysispipeline.folder import OutputFolder
import os

if os.path.exists(OutputFolder().path):
    cache_folder = OutputFolder().path
else:
    cache_folder = os.path.expanduser("~/tmp")
memory = Memory(cache_folder, verbose=100)
