.PHONY: html
html:
	cd doc; make clean; make html

dist/readme.pdf: doc/command_line_abc.rst
	rst2pdf $< -o $@

.PHONY: dist
dist:
	rm -rf dist/*
	python setup.py sdist

.PHONY: dist/doc
dist/doc: html
	cp -R doc/_build/html dist/doc

.PHONY: discriminatEM.zip
discriminatEM.zip: html dist dist/readme.pdf dist/doc
	rm -f discriminatEM.zip
	cd dist; zip -r ../discriminatEM.zip *
